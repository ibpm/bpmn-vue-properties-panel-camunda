import Conditional from '../../../part/detail/Conditional'
import elementHelper from '../../../../mixins/elementHelper'
import { getEventDefinition } from '../../../../utils'

export default {
  components: {
    Conditional
  },
  mixins: [elementHelper],
  methods: {
    saveConditional(props) {
      this.writeModdle(getEventDefinition(this.element), props)
    }
  }
}
