import Timer from '../../../part/detail/Timer'
import CollapseItem from '../../../ui/CollapseItem'
import elementHelper from '../../../../mixins/elementHelper'
import { getEventDefinition } from '../../../../utils'
import { createFormalExpression } from '../../../../utils/creators'

export default {
  components: {
    Timer,
    CollapseItem
  },
  mixins: [elementHelper],
  computed: {
    timer() {
      const data = {}, timerEventDefinition = getEventDefinition(this.element)
      if (timerEventDefinition) {
        if ('timeDate' in timerEventDefinition) {
          data.timerDefinitionType = 'timeDate'
        } else if ('timeDuration' in timerEventDefinition) {
          data.timerDefinitionType = 'timeDuration'
        } else if ('timeCycle' in timerEventDefinition) {
          data.timerDefinitionType = 'timeCycle'
        }
        data.timerDefinition = timerEventDefinition[data.timerDefinitionType]?.body
      }
      return data
    }
  },
  methods: {
    saveTimerDefinition(timer) {
      const properties = {}
      // 清空原类型的数据
      if (timer.oldType) {
        properties[timer.oldType] = undefined
      }
      // 设置新类型的数据
      if (timer.timerDefinitionType) {
        properties[timer.timerDefinitionType] = createFormalExpression(this.moddle, {
          body: timer.timerDefinition
        })
      }
      this.writeModdle(getEventDefinition(this.element), properties)
    }
  }
}
