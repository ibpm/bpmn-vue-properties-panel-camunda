import Escalation from '../../../part/detail/Escalation'
import elementHelper from '../../../../mixins/elementHelper'
import { getEventDefinition } from '../../../../utils'

export default {
  components: {
    Escalation
  },
  mixins: [elementHelper],
  computed: {
    msg() {
      return getEventDefinition(this.element)?.escalationRef?.name
    }
  },
  methods: {
    saveRef(escalation) {
      this.writeModdle(getEventDefinition(this.element), { escalationRef: escalation })
    },
    saveEscalation(props) {
      this.writeModdle(getEventDefinition(this.element), props)
    }
  }
}
