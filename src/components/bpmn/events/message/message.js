import Message from '../../../part/detail/Message'
import elementHelper from '../../../../mixins/elementHelper'
import { getEventDefinition } from '../../../../utils'

export default {
  components: {
    Message
  },
  mixins: [elementHelper],
  computed: {
    msg() {
      return getEventDefinition(this.element)?.messageRef?.name
    }
  },
  methods: {
    saveRef(message) {
      this.writeModdle(getEventDefinition(this.element), { messageRef: message })
    }
  }
}
