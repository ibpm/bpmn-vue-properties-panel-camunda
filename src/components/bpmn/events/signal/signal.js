import Signal from '../../../part/detail/Signal'
import elementHelper from '../../../../mixins/elementHelper'
import { getEventDefinition } from '../../../../utils'

export default {
  components: {
    Signal
  },
  mixins: [elementHelper],
  computed: {
    msg() {
      return getEventDefinition(this.element)?.signalRef?.name
    }
  },
  methods: {
    saveRef(signal) {
      this.writeModdle(getEventDefinition(this.element), { signalRef: signal })
    }
  }
}
